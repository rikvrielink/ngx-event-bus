import {Injectable} from '@angular/core';
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/map";

@Injectable()
export class EventBusService {


    private eventBus: Subject<Event>;

    constructor() {
        this.eventBus = new Subject<Event>();
    }

    public publish<T>(data: T): void {
        const event = new Event(data.constructor.name, data);
        this.eventBus.next(event);
    }

    public on<T>(type: T): Observable<T> {
        return this.eventBus.filter(e => e.name === type.constructor.name).map(e => e.data);
    }

}

class Event {
    constructor(private _name: string,
                private _data: any) {
    }


    get name(): string {
        return this._name;
    }

    get data(): any {
        return this._data;
    }
}