import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EventBusService} from "./event-bus.service";

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [],
    exports: [EventBusService]
})
export class EventBusModule {
}
